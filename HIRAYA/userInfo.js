//<!-- add javascript here -->
//<script type="text/javascript">
	// GETTING ALL INPUT TEXT FIELDS
	var username = document.forms["vForm"]["username"];
	var email = document.forms["vForm"]["email"];
	var password = document.forms["vForm"]["password"];
	var password_confirmation = document.forms["vForm"]["password_confirmation"];
	var firstName = document.forms["vForm"]["firstName"];
	var middleName = document.forms["vForm"]["middleName"];
	var lastName = document.forms["vForm"]["lastName"];

	// GETTING ALL ERROR OBJECTS
	var name_error = document.getElementById("name_error");
	var email_error = document.getElementById("email_error");
	var firstName_error = document.getElementById("firstName_error");
	var middleName_error = document.getElementById("middleName_error");
	var lastName_error = document.getElementById("lastName_error");
	var password_error = document.getElementById("password_error");

	// SETTING ALL EVENT LISTENERS
	username.addEventListener("blur", nameVerify, true);
	email.addEventListener("blur", emailVerify, true);
	firstName.addEventListener("blur", firstVerify, true);
	middleName.addEventListener("blur", middleVerify, true);
	lastName.addEventListener("blur", lastVerify, true);

	function Validate(){
		// VALIDATE USERNAME
		if(username.value == ""){
			name_error.textContent = "Username is required";
			username.style.border = "1px solid red";
			username.focus();
			return false;
		}
		
		if(firstName.value == ""){
			firstName_error.textContent = "First name is required";
			firstName.style.border = "1px solid red";
			firstName.focus();
			return false;
		}
		
		if(middleName.value == ""){
			middleName_error.textContent = "Middle name is required";
			middleName.style.border = "1px solid red";
			middleName.focus();
			return false;
		}
		
		if(lastName.value == ""){
			lastName_error.textContent = "Last name is required";
			lastName.style.border = "1px solid red";
			lastName.focus();
			return false;
		}
		
		// VALIDATE EMAIL
		if(email.value == ""){
			email_error.textContent = "Email is required";
			email.style.border = "1px solid red";
			email.focus();
			return false;
		}

		// VALIDATE PASSWORD
		if (password.value != password_confirmation.value) {
			confirmPass_error.textContent = "The two passwords do not match";
			password.style.border = "1px solid red";
			password_confirmation.style.border = "1px solid red";
			password.focus();
			return false;
		}

		// PASSWORD REQUIRED
		if (password.value == "" || password_confirmation.value == "") {
			confirmPass_error.textContent = "Password required";
			password.style.border = "1px solid red";
			password_confirmation.style.border = "1px solid red";
			password.focus();
			return false;
		}
		
		
	}

	// ADD EVENT LISTENERS
	function nameVerify(){
		if (username.value != "") {
			name_error.innerHTML = "";
			username.style.border = "1px solid #110E0F";
			return true;
		}
	}
	
	function firstVerify(){
		if (firstName.value != "") {
			firstName_error.innerHTML = "";
			firstName.style.border = "1px solid #110E0F";
			return true;
		}
	}
	
	function middleVerify(){
		if (middleName.value != "") {
			middleName_error.innerHTML = "";
			middleName.style.border = "1px solid #110E0F";
			return true;
		}
	}
	
	function lastVerify(){
		if (lastName.value != "") {
			lastName_error_error.innerHTML = "";
			lastName_error.style.border = "1px solid #110E0F";
			return true;
		}
	}
	
	function emailVerify(){
		if (email.value != "") {
			email_error.innerHTML = "";
			email.style.border = "1px solid #110E0F";
			return true;
		}
	}


//</script>